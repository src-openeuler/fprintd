Name:		fprintd
Version:	1.94.4
Release:	1
Summary:	D-Bus service for Fingerprint reader access

License:	GPL-2.0-or-later
Source0:	https://gitlab.freedesktop.org/libfprint/fprintd/-/archive/v%{version}/fprintd-v%{version}.tar.bz2
Url:		https://fprint.freedesktop.org/
ExcludeArch:    s390 s390x

BuildRequires: gcc
BuildRequires: meson >= 0.50.0
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(gio-2.0) >= 2.64
BuildRequires: pkgconfig(gio-unix-2.0) >= 2.64
BuildRequires: pkgconfig(glib-2.0) >= 2.64
BuildRequires: pkgconfig(gmodule-2.0) >= 2.64
BuildRequires: pkgconfig(libfprint-2) >= 1.94.0
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(polkit-gobject-1) >= 0.91
BuildRequires: pkgconfig(systemd) >= 235
BuildRequires: pam-devel
BuildRequires: gtk-doc
BuildRequires: gettext
BuildRequires: /usr/bin/pod2man
BuildRequires: /usr/bin/xmllint
BuildRequires: /usr/bin/xsltproc
BuildRequires: perl-podlators
BuildRequires: python3-dbusmock
BuildRequires: python3-libpamtest

Provides:         pam_fprint = %{version}-%{release} %{name}-pam = %{version}-%{release}
Obsoletes:        pam_fprint < 0.2-3 %{name}-pam < %{version}-%{release}

Requires(postun): authselect >= 0.3

%description
The fprint project aims to add support for consumer fingerprint reader devices, in Linux, as well as other free Unices.

%package devel
Summary:          Development package for %{name}
Requires:         %{name} = %{version}-%{release}
License:          GPL-2.0-or-later AND GFDL-1.1-or-later
BuildArch:        noarch

%description devel
This package contains some libraries and header files for the
development of %{name}.

%package_help

%prep
%autosetup -n %{name}-v%{version} -p1
mv pam/README pam-README

%build
%meson -Dgtk_doc=true -Dpam=true -Dpam_modules_dir=%{_libdir}/security
%meson_build

%install
%meson_install
mkdir -p $RPM_BUILD_ROOT/%{_localstatedir}/lib/fprint

%find_lang %{name}

%postun
if [ $1 -eq 0 ]
then
    /bin/authselect current | grep with-fingerprint >/dev/null 2>&1
    if [ $? -eq 0 ]
    then
        /bin/authselect disable-feature with-fingerprint || :
    fi
fi

%files -f %{name}.lang
%license COPYING
%doc README pam-README AUTHORS TODO
%{_libdir}/security/pam_fprintd.so
%{_bindir}/fprintd-*
%{_libexecdir}/fprintd
%{_sysconfdir}/fprintd.conf
%{_datadir}/dbus-1/system.d/net.reactivated.Fprint.conf
%{_datadir}/dbus-1/system-services/net.reactivated.Fprint.service
%{_datadir}/polkit-1/actions/net.reactivated.fprint.device.policy
%{_unitdir}/fprintd.service
%{_localstatedir}/lib/fprint

%files devel
%{_datadir}/gtk-doc/
%{_datadir}/dbus-1/interfaces/net.reactivated.Fprint.{Device,Manager}.xml

%files help
%{_mandir}/man1/fprintd.1*
%{_mandir}/man8/pam_fprintd.8*

%changelog
* Fri Sep 06 2024 Funda Wang <fundawang@yeah.net> - 1.94.4-1
- update to 1.94.4

* Thu Jun 13 2024 yao_xin <yao_xin001@hoperun.com> - 1.94.2-16
- License compliance rectification

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-15
- Fix loading external modules

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-14
- Translated using Weblate (Croatian)

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-13
- Translated using Weblate (Hungarian)

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-12
- Translated using Weblate (Georgian)

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-11
- Translated using Weblate (Thai)

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-10
- scripts: Update uncrustify configuration

* Fri Jan 06 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-9
- Translated using Weblate (Chinese (Simplified) (zh_CN))

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-8
- Translated using Weblate (Russian)

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-7
- Permit build without pam_wrapper

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-6
- meson: Add option to use libelogind for DBus

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-5
- tests: Hide intermediate error in output checker

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-4
- meson: Do not hard-require test dependencies

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-3
- tests: Fix dbusmock AddDevice calls to include optional argument

* Thu Jan 05 2023 xuxinyu <xuxinyu@xfusion.com> - 1.94.2-2
- Don't try to clear storage on devices without internal
- storage This change prevents warnings like 'Failed to clear
- storage before first enrollment: Device has no storage.'.

* Mon May 30 2022 chenchen <chen_aka_jan@163.com> - 1.94.2-1
- Update to 1.94.2

* Fri Jul 30 2021 linjiaxin5 <linjiaxin5@huawei.com> - 0.8.1-6
- Fix failure caused by GCC upgrade to 10

* Fri Feb 26 2021 lingsheng <lingsheng@huawei.com> - 0.8.1-5
- Disable with-fingerprint when with-fingerprint is enabled

* Mon Nov 04 2019 huzhiyu <huzhiyu1@huawei.com> - 0.8.1-4
- Package init
